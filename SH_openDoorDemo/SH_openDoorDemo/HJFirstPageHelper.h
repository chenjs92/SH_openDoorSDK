//
//  HJFirstPageHelper.h
//  SmartHome
//
//  Created by huiju on 15/3/26.
//  Copyright (c) 2015年 huiju. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncSocket.h"

@protocol HJFirstPageHelperDelegate <NSObject>


/**
 *  WiFi开门数据返回
 *
 *  @param dic 返回值
 */
- (void)didWiFiReturnWith:(NSData *)data;
@end

@interface HJFirstPageHelper:NSObject
@property (nonatomic,weak) id<HJFirstPageHelperDelegate> delegate;
@property (nonatomic ,strong)GCDAsyncSocket *asyncSocket;
//WIFI开门请求，已确认是否能够开门
-(void)wifiOpenRequse:(NSInteger)tag;
//在确认是WIFI开门之后，开门
-(void)openTheDoor;
//当为WIFI开门时，需要保存开门的记录
-(void)saveOpenRecord:(NSString *)doorId;
//当WIFI开门时，而又不是连接的该小区时，需要切换小区
-(void)changCommunityWiFiOpenDoor:(NSString *)vid;
@end
