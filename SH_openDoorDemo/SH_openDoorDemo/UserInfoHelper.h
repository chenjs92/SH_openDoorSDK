//
//  UserInfoHelper.h
//  SmartHome
//
//  Created by heihei on 15/8/7.
//  Copyright (c) 2015年 huiju. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfoHelper : NSObject

/**
 *  出事化用户信息类
 *
 *  @return <#return value description#>
 */
+ (UserInfoHelper *)share;



//保存用户信息
- (void)saveUserInfoWith:(NSMutableDictionary *)userInfoDic;
/**
 *  修改kuserInfo里面的信息
 *
 *  @param object 值
 *  @param key    健
 */
- (void)changeUserInfoOrBindInfoWith:(id)object forKey:(NSString*)key;

/**
 *  修改用户基本信息（user里面）
 *
 *  @param object                 值
 *  @param getUserInfoAndBindInfo 健
 */
- (void)changeUserInfoWithObject:(id)object forKey:(NSString*)key;
/**
 *  取出保存的用户信息和绑定信息
 *
 *  @return
 */
- (NSDictionary*)getUserInfoAndBindInfo;
/**
 *  获取用户名
 *
 *  @return
 */
- (NSString *)getUserName;
/**
 *  获得用户电话号码
 *
 *  @return
 */
- (NSString *)getPhoneNum;
/**
 *  获得用户id
 *
 *  @return
 */
- (NSString *)getUserId;
/**
 *  获取token
 *
 *  @return
 */
- (NSString *)getToken;
/**
 *  获得用户信息
 *
 *  @return
 */
- (NSDictionary *)getUserInfo;
/**
 *  获取绑定的房间
 *
 *  @return 
 */
- (NSArray*)getBindInfo;
-(Byte)getVerify:(Byte)protoType tag:(NSData *)tag data:(NSData *)data;

/**
 *  获取商城id
 *
 *  @return 商城id
 */
- (NSString *)getMallCode;

/**
 *  获取选中小区的id
 *
 *  @return
 */
- (NSString *)getSelectVillageId;

/**
 *  获取选中小区的信息
 *
 *  @return
 */
- (NSDictionary *)getSelectVillageInfo;

/**
 *  保存选中小区信息
 */
- (void)saveSelectVillageDic:(NSDictionary *)villageDic;

@end
