//
//  UserInfoHelper.m
//  SmartHome
//
//  Created by heihei on 15/8/7.
//  Copyright (c) 2015年 huiju. All rights reserved.
//

#import "UserInfoHelper.h"

@implementation UserInfoHelper

+ (UserInfoHelper *)share{
    
    static UserInfoHelper *sharedAccountManagerInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedAccountManagerInstance = [[self alloc] init];
    });
    
    return sharedAccountManagerInstance;
}


// 保存登陆用户的信息
- (void)saveUserInfoWith:(NSMutableDictionary *)userInfoDic{
    NSUserDefaults *userInfo = [NSUserDefaults standardUserDefaults];
    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:userInfoDic];
    //保存用户信息
    [userInfo setObject:userData forKey:kUserInfo];
    
    [userInfo synchronize];
}

// 修改缓存中的用户基本信息(user里面)
- (void)changeUserInfoWithObject:(id)object forKey:(NSString*)key
{
    NSMutableDictionary *userDic = [[self getUserInfoAndBindInfo] mutableCopy];
    NSMutableDictionary* userInfo = [[userDic objectForKey:@"user"] mutableCopy];
    [userInfo setObject:object forKeyedSubscript:key];
    [userDic setObject:userInfo forKey:@"user"];
    [self saveUserInfoWith:userDic];
}

// 修改第一层数据
- (void)changeUserInfoOrBindInfoWith:(id)object forKey:(NSString*)key
{
    NSMutableDictionary *userDic = [[self getUserInfoAndBindInfo] mutableCopy];
    if (!userDic) {
        userDic = [NSMutableDictionary dictionary];
    }
    [userDic setObject:object forKey:key];
    [self saveUserInfoWith:userDic];
}


// 取出保存的用户信息和绑定信息
- (NSDictionary*)getUserInfoAndBindInfo
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults valueForKey:kUserInfo];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    if (data) {
        @try {
            dict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            return dict;
        }
        @catch (NSException *exception) {
            // 本地用户信息为空 将本地的选择小区信息和登陆信息清空
            [userDefaults setBool:NO forKey:@"isLogin"];
            [userDefaults setObject:nil forKey:kSelectVillageInfo];
            [userDefaults synchronize];
            return dict;
        }
        @finally {
        }
    }else {
        // 本地用户信息为空 将本地的选择小区信息合登陆信息清空
        [userDefaults setBool:NO forKey:@"isLogin"];
        [userDefaults setObject:nil forKey:kSelectVillageInfo];
        [userDefaults synchronize];
    }
    
    return dict;
}

//获得用户名
- (NSString *)getUserName{
    NSDictionary *userInfoDic = [self getUserInfo];
    NSString *userName = [userInfoDic valueForKey:@"username"];
    
    return userName;
}

//电话号码
- (NSString *)getPhoneNum{
    
    NSDictionary *userInfoDic = [self getUserInfo];
    
    NSString *phone = [userInfoDic valueForKey:@"phone"];
    
    return phone;
}
//获得用户id
- (NSString *)getUserId{
    
    NSDictionary* userInfo = [self getUserInfo];
    NSString* uid = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"id"]];
    return uid;
}

// 获取token
- (NSString *)getToken {
    
    NSDictionary *userInfo = [self getUserInfo];
    NSString *token = [userInfo objectForKey:@"token"];
    return token;
}

/**
 *  获取用户信息
 *
 *  @return （NSMutableDictionary）
 */
- (NSDictionary *)getUserInfo{
    
    NSDictionary *userInfoDic = [self getUserInfoAndBindInfo];
    NSDictionary* userInfo = [userInfoDic objectForKey:@"user"];
    return userInfo;
}

/**
 *  获取绑定的房间
 *
 *  @return 绑定房间的数组
 */
- (NSArray*)getBindInfo {
  
    NSDictionary *userInfoDic = [self getUserInfoAndBindInfo];
    NSArray* bindInfo = [userInfoDic objectForKey:@"bind"];
    return bindInfo;
}


/**
 * 获取校验
 *
 * @param protoType 协议类型
 * @param tag       传输标识
 * @param data      数据
 * @return 校验字节
 */
-(Byte)getVerify:(Byte)protoType tag:(NSData *)tag data:(NSData *)data {
    Byte verify = protoType;
    // tag段
    Byte *tagData = (Byte *)[tag bytes];
    for (int i = 0; i < tag.length; ++i) {
        verify ^= tagData[i];
    }
    
    // 数据段
    Byte *dataData = (Byte *)[data bytes];
    for (int i = 0; i < data.length; ++i) {
        verify ^= dataData[i];
    }
    return verify;
}

/**
 *  获取商城id
 *
 *  @return 商城id
 */
- (NSString *)getMallCode {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *mallDic = [userDefaults objectForKey:kMallCode];
    if (mallDic) {
        // 有商城信息
        return [mallDic objectForKey:@"id"];
    }
    return nil;
}


/**
 *  获取选中小区的id
 *
 *  @return
 */
- (NSString *)getSelectVillageId {

    return [NSString stringWithFormat:@"%@",[[self getSelectVillageInfo] objectForKey:@"vid"]];
}

/**
 *  获取选中小区的信息
 *
 *  @return
 */
- (NSDictionary *)getSelectVillageInfo {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *villageDic = [userDefaults objectForKey:kSelectVillageInfo];
    return villageDic;
}

/**
 *  保存选中小区信息
 */
- (void)saveSelectVillageDic:(NSDictionary *)villageDic {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:villageDic forKey:kSelectVillageInfo];
    [userDefaults synchronize];
}

@end
