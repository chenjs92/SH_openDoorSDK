//
//  ViewController.m
//  SH_openDoorDemo
//
//  Created by ChenJs92 on 15/12/10.
//  Copyright © 2015年 huiJuZhiNeng. All rights reserved.
//

#import "ViewController.h"
#import "HJFirstPageHelper.h"
@interface ViewController ()<HJFirstPageHelperDelegate>{
     HJFirstPageHelper *firstPageHelper;
}
@property (nonatomic,strong) NSTimer *wifiOpenDoorTimer;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    firstPageHelper = [[HJFirstPageHelper alloc]init];
    firstPageHelper.delegate = self;
    
    NSDictionary *dic = @{@"alias":@"chenjs92",@"id":@"9776",@"phone":@"18324121337",@"token":@"b5f51f00267db75f680e62ada39f964f",@"username":@"chenjs92",@"validity":@"1452316938332"};
    [[UserInfoHelper share] changeUserInfoOrBindInfoWith:dic forKey:@"user"];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)openDoorAction:(id)sender {
    [firstPageHelper openTheDoor];
    [self.wifiOpenDoorTimer invalidate];
    self.wifiOpenDoorTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(wifiOpneDoorSendTime) userInfo:nil repeats:YES];
}
-(void)wifiOpneDoorSendTime
{
    [self.wifiOpenDoorTimer invalidate];
    NSLog(@"WiFi开门超时！");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
